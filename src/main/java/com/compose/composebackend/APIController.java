package com.compose.composebackend;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/builder")
public class APIController {

    @GetMapping("/message")
    public String getMessage() {
        return "Welcome to ComposeX Builder";
    }
}
