package com.compose.composebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComposeBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComposeBackendApplication.class, args);
	}

}
