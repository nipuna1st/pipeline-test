FROM openjdk:14-jdk-alpine
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} compose-backend-v0.0.1.jar
ENTRYPOINT ["java","-jar","/compose-backend-v0.0.1.jar"]
