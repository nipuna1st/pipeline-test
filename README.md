# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Run backend in local ###
1. Run the application from the main class

### Run using Docker ###
1. Build the project using gradle jar or build using compose-backend > Tasks > build > bootJar in the Gradle Tab
1. Execute the following command to build the docker image
    ```sh
    docker build -t compose-backend .
    ```
1. Execute following to run the docker image
    ```sh
    docker run -p 8080:8080 compose-backend   
    ```